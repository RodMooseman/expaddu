<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Infraestructura </title>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #Sol,#Pro,#Ger,#Dir,#Com,#AFin
    {
      width:120px;
      float:right;
    }
    th
    {
      font-size: 16px;
      border: 1px solid black;
      text-align: center;
    }
    td
    {
      text-align: center;
      font-size: 16px;
      border: 1px solid black;
    }
  </style>
  <?php
    include 'dbc.php';
    include 'session.php';
    $conn = mysqli_connect($host, $user, $pass, $db);
    if(! $conn )
      die('Conexion sql fallida!' /*. mysqli_error()*/);
    $vm=array('ecpu' =>0,'emem' =>0,'esd'=>0,'scpu' =>0,'smem' =>0,'ssd'=>0);
    if($_POST['Pro']!=""&&$_POST['Sol']!="")
    {
      $sql="select no,Req,FR,Ger,Dir,ASol,AFin,Apl,DA,TEI,Com,DP,PP,HO,NU,Ing,DR from proyectos where Pro='".$_POST['Pro']."' and Sol='".$_POST['Sol']."'";
      $re = mysqli_query($conn,$sql);
      $r=mysqli_affected_rows($conn);
      if($r>0)
      {
        $someData = mysqli_fetch_array($re);
        $sql="select SVCPU,SRAM,SSto,Sta,DVCPU,DRAM,DSto from maquinas where id=".$someData['no'];
        $re = mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r>0)
        {
          $pvm=0;
          $evm=0;
          while($row0 = mysqli_fetch_array($re))
          {
            $vm['ecpu'] += $row0['DVCPU'];
            $vm['emem'] += $row0['DRAM'];
            $vm['esd'] += $row0['DSto'];
            $vm['scpu'] += $row0['SVCPU'];
            $vm['smem'] += $row0['SRAM'];
            $vm['ssd'] += $row0['SSto'];
            if($row0['Sta']!="Pendiente"&&$row0['Sta']!="Pendiente sin recursos"&&$row0['Sta']!="Cancelado"&&$row0['Sta']!="Eliminada"&&$row0['Sta']!="Entregado a Romeo Piso6 KVM"&&$row0['Sta']!="Entregado a KVM Piso 6")
              $evm++;
            else if($row0['Sta']!="Cancelado"&&$row0['Sta']!="Eliminada")
              $pvm++;
          }
        }
        $sql="select SDQ,EDQ from ShaDQ where id=".$someData['no'];
        $re = mysqli_query($conn,$sql);
        $onlySDQ=array('SDQ' =>0,'EDQ' =>0);
        while($row55 = mysqli_fetch_array($re))
        {
          $onlySDQ['SDQ'] += $row55['SDQ'];
          $onlySDQ['EDQ'] += $row55['EDQ'];
        }
      }
    }
    else
    {
      $sql="select SVCPU,SRAM,SSto,DVCPU,DRAM,DSto,Sta from maquinas";
      $re = mysqli_query($conn,$sql);
      $r=mysqli_affected_rows($conn);
      if($r>0)
      {
        $pvm=0;
        $evm=0;
        while($row0 = mysqli_fetch_array($re))
        {
          $vm['ecpu'] += $row0['DVCPU'];
          $vm['emem'] += $row0['DRAM'];
          $vm['esd'] += $row0['DSto'];
          $vm['scpu'] += $row0['SVCPU'];
          $vm['smem'] += $row0['SRAM'];
          $vm['ssd'] += $row0['SSto'];
          if($row0['Sta']!="Pendiente"&&$row0['Sta']!="Pendiente sin recursos"&&$row0['Sta']!="Cancelado"&&$row0['Sta']!="Eliminada"&&$row0['Sta']!="Entregado a Romeo Piso6 KVM"&&$row0['Sta']!="Entregado a KVM Piso 6")
            $evm++;
          else if($row0['Sta']!="Cancelado"&&$row0['Sta']!="Eliminada")
            $pvm++;
        }
        $onlySDQ=array('SDQ' =>0,'EDQ' =>0);
        $sql="select SDQ,EDQ from ShaDQ";
        $re = mysqli_query($conn,$sql);
        while($row1 = mysqli_fetch_array($re))
        {
          $onlySDQ['SDQ'] += $row1['SDQ'];
          $onlySDQ['EDQ'] += $row1['EDQ'];
        }
      }
    }
  ?>
</head>
<body>
  <div class="container">
    <ul id="nav">
      <li><a href="<?php echo $lgout;?>">Cerrar sesion</a></li>
      <li>User : <?php echo $_COOKIE['myname'];?></li>
      <li clas="current"><a href="<?php echo $inside;?>">Menu</a></li>
      <li><a href="#" onclick="<?php echo "window.open('".$kill2."','','menubar=0,titlebar=0,width=400,height=400,resizable=0,left=40px,top=250px')";?>">Actualizar Registro</a></li>
      <li><?php echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>
      <li><a href="<?php echo $sols;?>">Solicitudes</a></li>
      <?php if($_COOKIE['lvl']==1){?>
      <li><a href="<?php echo $consulk;?>">Spec Ops</a></li><?php } ?>
    </ul>
    <br><br>
    <form method='post' action='cc.php' id='fist' >
    <br><br>
    <div class="lineR">
      <?php for($i=0;$i<20;$i++) echo "&nbsp;";?>Solicitante: <select name="Sol" id="Sol" onchange="this.form.submit()" >
      <option value="">- Solicitante -</option>
      <?php
        $re = mysqli_query($conn,"select upper(Sol) from proyectos group by upper(Sol)");
        $r=mysqli_affected_rows($conn);
        if($r<1)
          echo "<option value=\"\">No disponible</option> ";
        else
        while($row = mysqli_fetch_array($re))
        {
          $o ="<option";
          if($_POST['Sol']==$row['upper(Sol)'])
            $o .=" selected ";
          $o .=" value=\"".$row['upper(Sol)']."\">".$row['upper(Sol)']."</option>";
          echo $o;
        }
      ?>
      </select>
    </div>
    <div class = "lineR">
      <?php
        if($_POST['Sol']=="")
        {
        ?>
          <label>Proyecto:</label><select  id = "Pro" name = "Pro"  disabled = "disabled" required = "required">
            <option value = ""></option>
          </select>
        <?php
        }
        else
        {
          echo "Proyecto: <select name=\"Pro\" id=\"Pro\" onchange=\"this.form.submit()\">";
          echo "<option value=\"\">- Proyecto -</option>";
          $sql2="select Pro from proyectos where upper(Sol)='".$_POST['Sol']."'";
          $re2 = mysqli_query($conn,$sql2);
          $r2=mysqli_affected_rows($conn);
          if($r2<1)
            echo "<option value=\"\">No disponible</option> ";
          else
          while($row = mysqli_fetch_array($re2))
          {
            $o ="<option ";
            if($_POST['Pro']==$row['Pro'])
              $o .=" selected ";
            $o .=" value=\"".$row['Pro']."\">".$row['Pro']."</option>";
            echo $o;
          }
          echo "</select>";
        }
        mysqli_close($conn);
      ?>
    </div>
    <?php
      if($_POST['Pro']!="" && $_POST['Sol']!="")
      {
        ?>
        <br>
        <div style="margin-left:40px;"><br>
          <div class="lineR"><h3>Datos generales</h3></div>
          <div class="lineR"><h3>Secundarios</h3></div>
          <div class="lineR"><h3>Niveles de Riesgo</h3></div>
          <br><br>
          <div class="lineR">Fecha de recepcion :<div class="s1"><?php   echo $someData['FR'];?></div></div>
          <?php
            $amb=array('Desarrollo','QA','Produccion','Test','Pendiente','Pruebas','Otro','POC');
            $sol=explode("-",$someData['ASol']);
            $Solici="";
            for($i=0;$i<8;$i++)
            {
              if($sol[$i]=="1" && $Solici!="")
                $Solici .=", ".$amb[$i];
              if($sol[$i]=="1" && $Solici=="")
                $Solici .=$amb[$i];
            }
          ?>
          <div class="lineR">Ambiente solicitado :<div class="s1"><?php  echo $Solici;?></div></div>
          <div class="lineR">No. Semanas->implementacion :<div class="s1"><?php  echo $someData['TEI'];?></div></div>
          <br><br>
          <div class="lineR">Gerencia : <input type="text" id="Ger" name="Ger" value="<?php echo $someData['Ger'];?>" readonly="readonly"></div>
          <div class="lineR">
            Ambiente Entregado: <select name="AFin" id="AFin" >
            <option <?php if($someData['AFin'] == ''){echo("selected");}?> value=""></option>
            <?php
              $conn = mysqli_connect($host, $user, $pass, $db);
              $re = mysqli_query($conn,"select nombre from ambiente");
              if(! $re)
                echo "<option value=\"Pendiente\">Pendiente</option> ";
              else
                while($row = mysqli_fetch_array($re))
                {
                  $o ="<option ";
                  if($someData['AFin'] == $row['nombre'])
                    $o.=" selected ";
                  $o.="value=\"".$row['nombre']."\">".$row['nombre']."</option>";
                  echo $o;
                }
              mysqli_close($conn);
            ?>
            </select>
          </div>
          <div class="lineR">Horario de operacion:<div class="s1"><?php  echo $someData['HO'];?></div></div>
          <br><br>
          <div class="lineR">Direccion : <input type="text" id="Dir" name="Dir" value="<?php echo $someData['Dir'];?>" readonly="readonly"></div>
          <div class="lineR">Descripcion de proyecto :<div class="s1"><?php  echo $someData['DP'];?></div></div>
          <div class="lineR">No. Usuarios :<div class="s1"><?php  echo $someData['NU'];?></div></div>
          <br><br>
          <div class="lineR">Tipo de requerimiento :<div class="s1"><?php   echo $someData['Req'];?></div></div>
          <div class="lineR">Proovedor de proyecto :<div class="s1"><?php  echo $someData['PP'];?></div></div>
          <div class="lineR">Ingresos :<div class="s1"><?php  echo $someData['Ing'];?></div></div>
          <br><br>
          <div class="lineR">Aplicacion :<div class="s1"><?php   echo $someData['Apl'];?></div></div>
          <div class="lineR">Comentarios : <input type="text" id="Com" name="Com" value="<?php echo $someData['Com'];?>" readonly="readonly"></div>
          <div class="lineR">Disponibilidad requerida :<div class="s1"><?php  echo $someData['DR'];?></div></div>
          <br><br><br>
          <div class="lineR">
            <table style="display:inline;" >
              <tr>
                <th></th>
                <th style="font-size:15px;">Pendiente:</th>
                <th style="font-size:15px;">Entregado:</th>
                <th style="width:70px;font-size:15px;">Total:</th>
              </tr>
              <tr>
                <th style="font-size:15px;">VMs :</th>
                <td style="font-size:13px;"><?php echo $pvm; ?></td>
                <td style="font-size:13px;"><?php echo $evm; ?></td>
                <td style="width:70px;font-size:13px;"><?php echo $pvm+$evm; ?></td>
              </tr>
              <tr>
                <th style="font-size:15px;">&nbspDisco Compartido:&nbsp</th>
                <td style="font-size:13px;"><?php echo $onlySDQ['SDQ']-$onlySDQ['EDQ']; ?></td>
                <td style="font-size:13px;"><?php echo $onlySDQ['EDQ']; ?></td>
                <td style="width:70px;font-size:13px;"><?php echo $onlySDQ['SDQ']; ?></td>
              </tr>
              <tr>
                <th style="font-size:15px;">vCPU :</th>
                <td style="font-size:13px;"><?php echo $vm['scpu']-$vm['ecpu']; ?></td>
                <td style="font-size:13px;"><?php echo $vm['ecpu']; ?></td>
                <td style="width:70px;font-size:13px;"><?php echo $vm['scpu']; ?></td>
              </tr>
              <tr>
                <th style="font-size:15px;">RAM :</th>
                <td style="font-size:13px;"><?php echo $vm['smem']-$vm['emem']; ?></td>
                <td style="font-size:13px;"><?php echo $vm['emem']; ?></td>
                <td style="width:70px;font-size:13px;"><?php echo $vm['smem']; ?></td>
              </tr>
              <tr>
                <th style="font-size:15px;">Disco Duro  :</th>
                <td style="font-size:13px;"><?php echo $vm['ssd']-$vm['esd']; ?></td>
                <td style="font-size:13px;"><?php echo $vm['esd']; ?></td>
                <td style="width:70px;font-size:13px;"><?php echo $vm['ssd']; ?></td>
              </tr>
            </table>
          </div>
          <div class="lineR" >
            <?php
              echo "<script>var solic=".$someData['no'].";var w = screen.width;</script>";
              echo "<br><br><input type=\"button\" style=\"margin-left:55%;\" id=\"bt\" onclick=\"window.open('".$machk."?no='+solic,'','menubar=0,titlebar=0,width='+w+',height=400,resizable=0,left=40px,top=250px')\" value=\"Mostrar Maquinas Virtuales\" >";
              echo "<script>var some=".$someData['no'].";</script>";
            ?>
            <br><br><input type="button" id="Dia" style="margin-left:50%;" onclick="window.open('cleannames.php?no='+some+'&act=1','','menubar=0,titlebar=0,width=650,height=580,resizable=0,left=40px,top=50px')" value="Mostrar Diagrama Infraestructura" />
            </form>
          </div>
          <div class="lineR">
            <br>
          </div>
        </div>
        <?php
      }
      else
      {
        ?>
        <br><br><br><br>
        <table align="center">
          <tr>
            <th></th>
            <th>Pendiente:</th>
            <th>Entregado:</th>
            <th style="width:100px">Total:</th>
          </tr>
          <tr>
            <th>VMs :</th>
            <td><?php echo $pvm; ?></td>
            <td><?php echo $evm; ?></td>
            <td style="width:100px"><?php echo $pvm+$evm; ?></td>
          </tr>
          <tr>
            <th>&nbspDisco Compartido:&nbsp</th>
            <td><?php echo $onlySDQ['SDQ']-$onlySDQ['EDQ']; ?></td>
            <td><?php echo $onlySDQ['EDQ']; ?></td>
            <td style="width:100px"><?php echo $onlySDQ['SDQ']; ?></td>
          </tr>
          <tr>
            <th>vCPU :</th>
            <td><?php echo $vm['scpu']-$vm['ecpu']; ?></td>
            <td><?php echo $vm['ecpu']; ?></td>
            <td style="width:100px"><?php echo $vm['scpu']; ?></td>
          </tr>
          <tr>
            <th>RAM :</th>
            <td><?php echo $vm['smem']-$vm['emem']; ?></td>
            <td><?php echo $vm['emem']; ?></td>
            <td style="width:100px"><?php echo $vm['smem']; ?></td>
          </tr>
          <tr>
            <th>Disco Duro  :</th>
            <td><?php echo $vm['ssd']-$vm['esd']; ?></td>
            <td><?php echo $vm['esd']; ?></td>
            <td style="width:100px"><?php echo $vm['ssd']; ?></td>
          </tr>
        </table>
        <div align="center">
        </form>
          <br><br>
          <br>
        </div>
        <?php
      }
    ?>
    <br><br><br><br>
    <button type="button" class="evilbtn">Tecnologias Cloud</button>
    <br><br><br><br><br><br>
  </div>
  </body>
</html>